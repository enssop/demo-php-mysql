<!DOCTYPE html>
<html lang="en">
<?php if (isset($_POST['submit'])) {
    //Si l'utilisateur a soumis le formulaire, on inclus la connexion à la base de données
    require_once("db.php");
    //Verification que au moins un input n'est pas vide et que tous les champs non nuls de la table ne sont pas vide
    if (!empty($_POST['lname']) || !empty($_POST['fname']) || !empty($_POST['phone']) || !empty($_POST['mail']) || !empty($_POST['objet']) || !empty($_POST['message'])) {
        //Si l'utilisateur a rempli tous les champs obligatoire, on sanitise les saisies utilisateurs
        $lname = htmlspecialchars(trim($_POST['lname']));
        $fname = htmlspecialchars(trim($_POST['fname'])); 
        $phone = htmlspecialchars(trim($_POST['phone']));
        $email = htmlspecialchars(trim($_POST['mail']));
        $objet = htmlspecialchars(trim($_POST['objet']));
        $message = htmlspecialchars(trim($_POST['message']));
        $date_heure = date('Y-m-d H:i:s');
        $ip = !empty($_SERVER['HTTP_CLIENT_IP']) ?
         $_SERVER['HTTP_CLIENT_IP'] : 
         $_SERVER['REMOTE_ADDR'];

        //Insertion dans la base de données avec une requête préparée qui permet d'éviter les injections SQL
        $stmt = $db->prepare("INSERT INTO contact(prenom, nom, telephone, email, objet, `message`, date_heure, ip) VALUES (:lname, :fname, :phone, :mail, :objet, :messages, :heure, :ip);");
        $stmt->execute([
            'lname' => $lname,
            'fname' => $fname,
            'phone' => $phone,
            'mail'  => $email,
            'objet' => $objet,
            'messages'  => $message,
            'heure' => $date_heure,
            'ip' => $ip,
        ]);
    } else {
        //si l'utilisateur n'a pas rempli tous les champs obligatoires
    }
} ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Vous souhaitez nous contacter</h1>
    <p>Veuillez remplir le formulaire ci-dessous:</p>
    <form action="form.php" method="POST">
        <label for="lname">Nom de famille :<br>
            <input type="text" name="lname" id="lname" maxlength="50" placeholder="Nom de famille">
        </label><br>
        <label for="fname">Prénom :<br>
            <input type="text" name="fname" id="fname" placeholder="Prénom">
        </label><br>
        <label for="phone">Numéro de téléphone :<br>
            <input type="tel" name="phone" id="phone" placeholder="01-01-01-01-01">
        </label><br>
        <label for="mail">Email :<br>
            <input type="email" name="mail" id="mail">
        </label><br>
        <label for="objet">Objet de votre message :<br>
            <input type="text" name="objet" id="objet">
        </label> <br>
        <label for="message">Votre message :<br>
            <textarea name="message" id="message" cols="30" rows="8"></textarea>
        </label><br>
        <input type="submit" name="submit" value="Envoyer">
    </form>
</body>

</html>