<!DOCTYPE html>
<html lang="en">
<?php  //inclus le fichier PHP contenant la connexion à la base de données
require_once("db.php");
if (isset($_GET["id"])) {
    //Si l'id est dans l'url, récupération des information de contact avec cet id
    $stmt = $db->prepare('SELECT * FROM contact WHERE id = :id ;');
    $stmt->execute([
        'id' => $_GET['id'],
    ]);
    $contact = $stmt->fetch();
    if ($contact == false) {
        //Si l'id n'existe pas en base de donnée, l'utilisateur est redirigé vers la page index
        header('Location:/index.php');
    }
} else if (isset($_POST['submit'])) {

    if (!empty($_POST['lname']) || !empty($_POST['fname']) || !empty($_POST['phone']) || !empty($_POST['mail']) || !empty($_POST['objet']) || !empty($_POST['message'])) {
        //Si l'utilisateur a rempli tous les champs obligatoire, on sanitise les saisies utilisateurs
        $lname = htmlspecialchars(trim($_POST['lname']));
        $fname = htmlspecialchars(trim($_POST['fname']));
        $phone = htmlspecialchars(trim($_POST['phone']));
        $email = htmlspecialchars(trim($_POST['mail']));
        $objet = htmlspecialchars(trim($_POST['objet']));
        $message = htmlspecialchars(trim($_POST['message']));
        $id = $_POST['id'];

        //Insertion dans la base de données avec une requête préparée qui permet d'éviter les injections SQL
        $stmt = $db->prepare("UPDATE contact SET prenom = :fname, nom = :lname, telephone = :phone, email = :mail, objet = :objet, `message` = :messages WHERE id=:id;");
        $stmt->execute([
            'lname' => $lname,
            'fname' => $fname,
            'phone' => $phone,
            'mail'  => $email,
            'objet' => $objet,
            'messages'  => $message,
            'id' => $id,
        ]);
        header('Location:/index.php');
    } else {
        //si l'utilisateur n'a pas rempli tous les champs obligatoires
    }
} else {
    //Si l'utilisateur essaye d'accéder à la page detail sans passer par les liens, il est redirigé sur la page index
    header('Location:/index.php');
}
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <a href="/index.php">Retour à la liste de contacts</a>
    <!-- Affichage des information de contcat -->
    <h1>Modification du contact n°<?= $contact['id']; ?></h1>
    <form action="update.php" method="POST">
        <input type="hidden" name="id" value="<?= $contact['id']; ?>">
        <label for="lname">Nom de famille :<br>
            <input type="text" name="lname" id="lname" maxlength="50" placeholder="Nom de famille" value="<?= $contact['nom']; ?>">
        </label><br>
        <label for="fname">Prénom :<br>
            <input type="text" name="fname" id="fname" placeholder="Prénom" value="<?= $contact['prenom']; ?>">
        </label><br>
        <label for="phone">Numéro de téléphone :<br>
            <input type="tel" name="phone" id="phone" placeholder="01-01-01-01-01" value="<?= $contact['telephone']; ?>">
        </label><br>
        <label for="mail">Email :<br>
            <input type="email" name="mail" id="mail" value="<?= $contact['email']; ?>">
        </label><br>
        <label for="objet">Objet de votre message :<br>
            <input type="text" name="objet" id="objet" value="<?= $contact['objet']; ?>">
        </label> <br>
        <label for="message">Votre message :<br>
            <textarea name="message" id="message" cols="30" rows="8"><?= $contact['message']; ?></textarea>
        </label><br>
        <input type="submit" name="submit" value="Envoyer">
    </form>

</body>

</html>