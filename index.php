<!DOCTYPE html>
<html lang="en">
<?php  //inclus le fichier PHP contenant la connexion à la base de données
require_once("db.php");

//Si l'utilisateur a appuyé sur le bouton de suppression, supprime l'enregistrement dans la base de données
if (isset( $_POST['supprimer'])){
    $stmt_del = $db->prepare('DELETE FROM contact WHERE id= :id');
    $stmt_del->execute([
        'id' => $_POST['id']
    ]);
}

//Récupere tous les enregistrements dans la table contact
$stmt = $db->prepare('SELECT * FROM contact;');
$stmt->execute();
$contacts = $stmt->fetchAll();

 ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Démo php mysql</title>

</head>
<body>
    <h1>Contacts provenant du formulaire du site</h1>
    <table>
        <tbody>
            <tr>
                <th>Nom prénom</th>
                <th>Téléphone</th>
                <th>Email</th>
                <th>Objet</th>
                <th></th>
            </tr>
            <?php
            //Affichage des informations récupérées dans la table contact
            foreach($contacts as $contact){ ?>
                <tr>
                    <td><?= $contact["nom"].' '.$contact["prenom"]; ?></td>
                    <td><?= $contact["telephone"]; ?></td>
                    <td><?= $contact["email"]; ?></td>
                    <td><?= $contact["objet"]; ?></td>
                    <td>
                        <!-- lien vers la page détails qui affiche plus d'informations sur le contact -->
                        <a href="/detail.php?id=<?= $contact["id"] ?>">+ d'infos</a>
                        <a href="/update.php?id=<?= $contact["id"] ?>">Modifier</a>
                        <form method="POST">
                            <input type="hidden" name="id" value="<?= $contact["id"] ?>">
                            <input type="submit" name="supprimer" value="supprimer">
                        </form>
                    </td> 
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>