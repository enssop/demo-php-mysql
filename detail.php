<!DOCTYPE html>
<html lang="en">
<?php  //inclus le fichier PHP contenant la connexion à la base de données
require_once("db.php");
if  (   isset( $_GET["id"] )  ) {
    //Si l'id est dans l'url, récupération des information de contact avec cet id
    $stmt = $db->prepare('SELECT * FROM contact WHERE id = :id ;');
    $stmt->execute([
        'id' => $_GET['id'],
    ]);
    $contact = $stmt->fetch();
    if ($contact == false){
        //Si l'id n'existe pas en base de donnée, l'utilisateur est redirigé vers la page index
        header('Location:/index.php');
    }
} else {
    //Si l'utilisateur essaye d'accéder à la page detail sans passer par les liens, il est redirigé sur la page index
    header('Location:/index.php');
}
 ?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="/index.php">Retour à la liste de contacts</a>
    <!-- Affichage des information de contcat --> 
    <h1>Détail du contact n°<?= $contact['id']; ?></h1>
    <p>
        <strong>Nom :</strong>
        <?= $contact['nom']; ?>
    </p>
    <p>
        <strong>Prénom :</strong>
        <?= $contact['prenom']; ?>
    </p>
    <p>
        <strong>Téléphone :</strong>
        <a href="tel:<?= $contact['telephone']; ?>">
            <?= $contact['telephone']; ?>
        </a>
    </p>
    <p>
        <strong>Email :</strong>
        <a href="mailto:<?= $contact['email']; ?>">
            <?= $contact['email']; ?>
        </a>
    </p>
    <p>
        <strong>Objet :</strong>
        <?= $contact['objet']; ?>
    </p>
    <p>
        <strong>Message :</strong>
        <?= $contact['message']; ?>
    </p>
    <p>
        <strong>Date de prise de contact :</strong>
        <?php //Transformation de la date en une date lisible par un utilisateur
        $date_contact = date('d M Y H:i', strtotime($contact['date_heure']));
        echo $date_contact; ?>
    </p>
    <p>
        <strong>IP :</strong>
        <?= $contact['ip']; ?>
    </p>
</body>
</html>